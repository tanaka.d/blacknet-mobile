import 'package:blacknet/utils/sha512.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('SHA512 test', (){
    ///
    /// SHA512
    ///
    final String matcher = "152257c71e20e4e4268caf9314fbbb5d764dcac26839a1be47cf47eb9bc0928a83a19234f018f00e1dc476f51349a8d2d8037e2987c9ceda7dc6ba5f2ba9d3ab";
    final String plainText = "blacknet is perfect";    
    var hash = Sha512Util.hash(plainText);
    expect(hash, matcher);
  });
}
