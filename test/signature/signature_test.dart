import 'package:blacknet/models/index.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Signature test', (){
    ///
    /// Signature
    ///
    
    // piano maze provide discover tower scissors true leave senior aware secret film
    // blacknet14w6tm25y7rt24zj7r8fq7rnzd50qtpgmpfwv50r7qjnqhcwlxszqanh036
    var testAccount = "blacknet14w6tm25y7rt24zj7r8fq7rnzd50qtpgmpfwv50r7qjnqhcwlxszqanh036";
    var testMnemonic = "piano maze provide discover tower scissors true leave senior aware secret film";
    var message = "BLN-is-very-nice";

    var expectedSignature = "FF6D74C0493720F59DA4F06CD6D13D4A47D04F61E6D4AFF3D001EBD59153DC6DF40DDA34F7CD63FBA76CD2C1CA3963D1CB4F17F2061FB191BA441F0BF925C40B";

    var hexSignature = Bln.signMessage(testMnemonic, message);
    print("$hexSignature");
    expect(hexSignature, expectedSignature);

    expect(Bln.verifyMessage(testAccount, hexSignature, message), true);

  });
}
