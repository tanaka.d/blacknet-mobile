import 'dart:developer';
import 'dart:io';

import 'package:blacknet/utils/aes.dart';
import 'package:blacknet/utils/sha512.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

///
/// Singleton Crypto Component
/// 
class Crypto {

  static final Crypto _instance = Crypto.internal();
 
  factory Crypto() => _instance;

  Crypto.internal();

  final storage = FlutterSecureStorage();

  Future<String> getIdentifier() async {
    String identifier = "unknown";
    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      var android = await deviceInfoPlugin.androidInfo;
      log('${android.androidId}');
      identifier = android.androidId;
    } else if (Platform.isIOS) {
      var iOS = await deviceInfoPlugin.iosInfo;
      // identifierForVendor is more safety than UDID
      log('${iOS.identifierForVendor}');
      identifier = iOS.identifierForVendor;
    }
    return identifier;
  }

  Future<String> _getFormatedHashPassword(String password) async {
    var identifier = await getIdentifier();
    var formatedPassword = identifier + "-" + password;
    String hashPassword = Sha512Util.hash(formatedPassword).substring(0, 16);
    return hashPassword;
  }

  void putString(String key, String value, [String password]) async {
    // the value was encrypted by AES with hash password
    // a suffix(salt) was added to password
    // algo is :
    //    encryptedBase64Value = AES(value, SHA512(DEVICE_IDENTIFIER + "-" + password).substring)
    // besides, the encryptedBase64Value is stored in Keychain(iOS)/KeyStore(Android)
    if (null == password) {
      password = "";
    }
    var hashPassword = await _getFormatedHashPassword(password);
    String encrypted = AESUtil.encryptToBase64(value, hashPassword);
    await storage.write(key: key, value: encrypted);
  }

  Future<String> getString(String key, [String password]) async {
    if (null == password) {
      password = "";
    }
    var hashPassword = await _getFormatedHashPassword(password);
    String encrypted = await storage.read(key: key);
    return AESUtil.decryptFromBase64(encrypted, hashPassword);
  }

  void test() async {
    final String plainText = "blacknet is perfect";
    final String keyText = "00000000000x7c00";

    final String fieldName = "THAT_IS_GOOD";

    Crypto().putString(fieldName, plainText, keyText);

    var loaded = await Crypto().getString(fieldName, keyText);
    log(loaded);

    assert(loaded == plainText);
    log("Test Passed");
  }

}