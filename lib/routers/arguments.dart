import 'package:blacknet/models/index.dart';

class AccountPageArguments {
  final String password;
  final String mnemonic;
  final String address;
  AccountPageArguments({this.password, this.mnemonic, this.address});
}

class TxDetailPageArguments {
  final BlnTxns bln;
  TxDetailPageArguments({this.bln});
}

class WebviewArguments {
  final String title;
  final String url;
  WebviewArguments({this.title, this.url});
}