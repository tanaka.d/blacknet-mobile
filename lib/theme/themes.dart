// 枚举功能方法
enum Themes {
  darker,
  red,
  teal,
  blue,
  green,
  indigo,
  purple,
  deepPurple
}