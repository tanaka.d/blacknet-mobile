import 'package:flutter/material.dart';

import 'package:majascan/majascan.dart';
import 'package:qr_flutter/qr_flutter.dart';

import 'package:blacknet/utils/logo.dart';
import 'package:blacknet/utils/toast.dart';
import 'package:blacknet/utils/validator.dart';
import 'package:blacknet/res/index.dart';

class QrcodeHelper {
  static Future<String>  scanQrAddress() async {

    String barcodeScanRes;

    try { 
      barcodeScanRes = await MajaScan.startScan(
        title: "QRcode Scanner", 
        barColor: Colors.transparent, 
        titleColor: Colours.grey, 
        qRCornerColor: Colours.gold,
        qRScannerColor: Colours.gold,
        flashlightEnable: true
      );
    } catch (e) {
      Toast.show("扫描失败");
    }

    if (barcodeScanRes == null) return null;

    List<String> ss = barcodeScanRes.split(":");

    if(validatorAddress(ss[1])){
      return ss[1];
    }
    
    return null;
  }

  static Widget generateQRcode(String text, [double size]){
    return FutureBuilder(
      future: loadOverlayLogoImage(),
      builder: (ctx, snapshot) {
        final size = 280.0;
        if (!snapshot.hasData) {
          return Container(width: size, height: size);
        }
        return CustomPaint(
          size: Size.square(size),
          painter: QrPainter(
            data: text,
            version: QrVersions.auto,
            color: Colours.grey,
            // emptyColor: Colors.grey,
            // size: 320.0,
            embeddedImage: snapshot.data,
            embeddedImageStyle: QrEmbeddedImageStyle(
              size: Size.square(50),
            ),
          ),
        );
      },
    );
  }
}