import 'package:blacknet/models/index.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

class TxDbHelper {

  static final TxDbHelper _instance = new TxDbHelper.internal();

  factory TxDbHelper() => _instance;

	static Database _db;                // Singleton Database

	String dbTable = 'txns_table';
	String colFee = 'fee';
	String colFrom = 'fromAddress';
  String colTo = 'toAddress';
  String colAmount = 'amount';
  String colMessage = 'message';
  String colSeq = 'seq';
  String colSignature = 'signature';
  String colSize = 'size';
  String colTime = 'time';
  String colType = 'type';
  String colBlockHash = 'blockHash';
  String colBlockHeight = 'blockHeight';
  String colTxid = 'txid';

  TxDbHelper.internal();

	Future<Database> get db async {
		if (_db == null) {
			_db = await initializeDatabase();
		}
		return _db;
	}

	Future<Database> initializeDatabase() async {
    // Get the directory path for both Android and iOS to store database.
		Directory directory = await getApplicationDocumentsDirectory();
		String path = directory.path + '/blacknet10000.db';
    // await deleteDatabase(path);
		// Open/create the database at a given path
		Database dbs = await openDatabase(path, version: 1, onCreate: _createDb);
    return dbs;
	}

	void _createDb(Database _db, int newVersion) async {
    await _db.execute('CREATE TABLE $dbTable ($colTxid TEXT PRIMARY KEY UNIQUE, $colFee INTEGER, '
				'$colFrom TEXT, $colTo TEXT, $colBlockHeight INTEGER, $colBlockHash TEXT, $colSignature TEXT, $colAmount REAL, $colMessage TEXT, $colSeq INTEGER, $colSize INTEGER, $colTime TEXT, $colType INTEGER)');
  }

  // insertUpdate Operation
	Future<int> insertUpdate(BlnTxns tx) async {
    BlnTxns t = await one(tx.txid);
    if(t != null){
      //update
      return await update(tx);
    }else{
      //insert
      return await insert(tx);
    }
	}

  // insertUpdate Operation
	insertUpdates(List<BlnTxns> txs) async {
    txs.forEach((tx) async{
      await insertUpdate(tx);
    });
	}

  // Insert Operation
	Future<int> insert(BlnTxns tx) async {
		Database client = await this.db;
		// var result = await client.insert(dbTable, tx.toMap());
		// return result;
    return await client.insert(dbTable, tx.toMap());
	}

  // one Operation
	Future<BlnTxns> one(String txid) async {
		Database client = await this.db;
    List<Map> result = await client.query(dbTable,
        where: '$colTxid = ?',
        whereArgs: [txid]);
    if (result.length > 0) {
      return BlnTxns.fromMap(result.first);
    }
    return null;
	}

  // query Operation
	Future<List<BlnTxns>> query(String address, [int page, int type]) async {
		Database client = await this.db;
    String where = '$colFrom = ? or $colTo = ?';
    List whereArgs = [address, address];
    int offset = 0;
    if(type != null){
      where = '($colFrom = ? or $colTo = ?) and $colType = ?';
      whereArgs = [address, address, type];
    }
    if(page != null){
      offset = (page - 1) * 100;
    }
		List<Map> result = await client.query(dbTable,
        where: where,
        whereArgs: whereArgs,
        orderBy: '$colTime Desc',
        limit: 100,
        offset: offset
    );
    List<BlnTxns> txs = [];
    if (result.length > 0) {
      result.forEach((r){
        txs.add(BlnTxns.fromMap(r));
      });
    }
    return txs;
	}

  // count Operation
	Future<int> count(String address, [int type]) async {
		Database client = await this.db;
    List whereArgs = [address, address];
    String sql = 'SELECT COUNT(*) FROM $dbTable WHERE $colFrom = ? OR $colTo = ?';
    if(type != null){
      sql = 'SELECT COUNT(*) FROM $dbTable WHERE $colFrom = ? OR $colTo = ? AND $colType = ?';
      whereArgs = [address, address, type];
    }
    return Sqflite.firstIntValue(
        await client.rawQuery(sql, whereArgs));
	}

  // Update Operation: Update a todo object and save it to database
	Future<int> update(BlnTxns tx) async {
		Database client = await this.db;
    return await client.update(dbTable, tx.toMap(), where: '$colTxid = ?', whereArgs: [tx.txid]);
	}

  // Delete Operation: Delete a object from database
	Future<int> delete(String txid) async {
		Database client = await this.db;
    return await client.rawDelete('DELETE FROM $dbTable WHERE $colTxid = $txid');
	}

  Future close() async {
    var dbClient = await db;
    return dbClient.close();
  }
}