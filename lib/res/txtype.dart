class TxType {
  static const Transfer = 0;
  static const Burn = 1;
  static const Lease = 2;
  static const CancelLease = 3;
  static const Bundle = 4;
  static const CreateHTLC = 5;
  static const UnlockHTLC = 6;
  static const RefundHTLC = 7;
  static const SpendHTLC = 8;
  static const CreateMultisig = 9;
  static const SpendMultisig = 10;
  static const WithdrawFromLease = 11;
  static const ClaimHTLC = 12;
  static const MultiData = 16;
  static const Generated = 254;
}
