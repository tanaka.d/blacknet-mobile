// import 'package:flutter/material.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter_qr_reader/qrcode_reader_view.dart';

// class QRcodeScan extends StatefulWidget {
//   QRcodeScan({Key key}) : super(key: key);

//   @override
//   _QRcodeScanState createState() => new _QRcodeScanState();
// }

// class _QRcodeScanState extends State<QRcodeScan> {
//   GlobalKey<QrcodeReaderViewState> _key = GlobalKey();
//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       body: QrcodeReaderView(
//         key: _key,
//         onScan: onScan,
//         headerWidget: AppBar(
//           backgroundColor: Colors.transparent,
//           elevation: 0.0,
//         ),
//       ),
//     );
//   }

//   Future onScan(String data) async {
//     print(data);
//     await showCupertinoDialog(
//       context: context,
//       builder: (context) {
//         return CupertinoAlertDialog(
//           title: Text("扫码结果"),
//           content: Text(data),
//           actions: <Widget>[
//             CupertinoDialogAction(
//               child: Text("确认"),
//               onPressed: () => Navigator.pop(context),
//             )
//           ],
//         );
//       },
//     );
//     _key.currentState.startScan();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//   }
// }