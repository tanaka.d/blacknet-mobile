import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'button.dart';

import 'package:blacknet/common/index.dart';

// BottomSheetPay .
class BottomSheetPay extends StatefulWidget {

  Function onCancelTap;
  Function onTap;
  Function faceValidate;

  String from; //from
  String to; //to
  double amount;
  double fee;
  String memo;
  bool encrypted;
  BlnSendType type;

  BottomSheetPay({
    this.onCancelTap,
    this.onTap,
    this.from,
    this.to,
    this.amount,
    this.fee,
    this.memo,
    this.type,
    this.encrypted,
    this.faceValidate
  });
  @override
  _BottomSheetPayState createState() => _BottomSheetPayState();
}

class _BottomSheetPayState extends State<BottomSheetPay> {

  int step = 1;
  final TextEditingController _textController = TextEditingController();
  bool _nextStepStatus = false;
  Widget _errorText = errorText(null);
  final _formKey = GlobalKey<FormState>();
  bool _enableLocalAuth = true;
  bool _obscureText = true;

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 200)).then((v){
      setState(() {
        step = 2;
      });
    });

    // init form listener
    _textController.addListener((){
      setState(() {
        _nextStepStatus = _textController.text.length > 5;
        _errorText = errorText(null);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          AppBar(
            backgroundColor: Theme.of(context).bottomAppBarColor,
            title: navTitle(),
            titleSpacing: 0,
            automaticallyImplyLeading:false,
            elevation: 0,
            leading: navLeftButton(),
            actions: <Widget>[
              IconButton( 
                icon: new Icon(_enableLocalAuth && !!bln.localAuth ? Icons.security : Icons.face, color: Theme.of(context).accentColor),
                onPressed: () async {
                  if (_enableLocalAuth && !!bln.localAuth) {
                    // switch password
                    setState(() {
                      step = 3;
                      _enableLocalAuth = false;
                    });
                  } else {
                    // switch faceid
                    if (!bln.localAuth) {
                      // if dont open faceid， go to switch page
                      await OkNavigator.push(context, Routes.switchAuth);
                    }
                    setState(() {
                      step = 2;
                      _enableLocalAuth = !!bln.localAuth;
                    });
                  }          
                },
              )
            ],
          ),
          Expanded(
            flex: 1,
            child: (){
              if (step == 2) {
                return payInfo();
              }
              if (step == 3) {
                return inputPassword();
              }
              return loadInfo();
            }(),
          ),
          Container(
            height: 50,
            padding: EdgeInsets.only(left: 15, right: 15, bottom: 10),
            child: nextButton()
          )
        ],
      ),
    );
  }

  Widget nextButton(){
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    switch (step) {
      case 2:
        // faceid
        Function onTap = (){
          setState(() {
            step = 3;
          });
        };
        String text = S.of(context).nextStep;
        if (_enableLocalAuth && bln.localAuth) {
          onTap = () async {
            //show faceid and transfer bln
            if ( 
                widget.faceValidate != null && 
                await widget.faceValidate() && 
                widget.onTap != null
            ) {
              widget.onTap();
            }
          };
          text = S.of(context).confirm;
        }
        return MyButton(
          text: text, 
          onPressed: onTap
        );
      case 3:
        // password
        Function onTap;
        if (_nextStepStatus) {
          onTap = () async {
            if (validator() && widget.onTap != null) {
              widget.onTap();
            }
          };
        }
        return RaisedButton(
          child: Container(
            height: 40,
            width: double.infinity,
            alignment: Alignment.center,
            child: Text(S.of(context).confirm, style: TextStyle(fontSize: Dimens.font_sp18)),
          ), 
          onPressed: onTap
        );
      default:
        return Gaps.empty;
    }
  }

  Widget navLeftButton(){
    return IconButton( 
        icon: new Icon(step < 3 ? Icons.cancel : Icons.arrow_back, color: Theme.of(context).accentColor),
        onPressed: () {
          if (step < 3) {
            widget.onCancelTap();
          }else{
            setState(() {
              _textController.text = null;
              step = 2;
            });
          }
        }
    );
  }

  Widget navTitle(){
    if (step < 3) {
      return Text(S.of(context).detail);
    }
    return Text(S.of(context).inputPassword);
  }
  

  Widget loadInfo() {
    return Center(
      child: CircularProgressIndicator(strokeWidth: 3.0, valueColor: AlwaysStoppedAnimation<Color>(Colours.gold))
    );
  }

  Widget payInfo() {
    var list = <Widget>[];
    var receiveItem = ListTile(
      title: Text(widget.to,
        style: TextStyle(
          fontSize: 12
        )
      ),
      leading: Text(S.of(context).toAddress)
    );
    var payItem = ListTile(
      title: Text(widget.from,
        style: TextStyle(
          fontSize: 12
        )
      ),
      leading: Text(S.of(context).fromAddress)
    );
    var feeItem = ListTile(
      title: Text(widget.fee.toString() +" BLN",
        style: TextStyle(
          fontSize: 12
        )
      ),
      leading: Text(S.of(context).fee)
    );
    var infoItemTitle = getBlnSentType();
    var infoItem = ListTile(
      title: infoItemTitle,
      isThreeLine: false,
      leading: Text(S.of(context).transferInfo)
    );
    var titleItem = Container(
      padding: EdgeInsets.only(top: 0, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text(widget.amount.toString(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
              color: Theme.of(context).accentColor,
              fontWeight: FontWeight.bold
            )
          ),
          SizedBox(width: 5),
          Text("BLN",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 14,
            ),
          )
        ]
      )
    );
    list.add(titleItem);
    if (infoItemTitle != null) {
      list.add(infoItem);
    }
    list.add(receiveItem);
    list.add(payItem);
    list.add(feeItem);
    if (widget.memo != null) {
      var remarkItem = ListTile(
        title: Text(widget.memo,
          style: TextStyle(
            fontSize: 12
          )
        ),
        leading: Text(S.of(context).memo)
      );
      list.add(remarkItem);
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: list,
    );
  }

  Widget getBlnSentType(){
    switch (widget.type) {
      case BlnSendType.cancelLease:
        return Text("BLN "+S.of(context).cancelLease,
          style: TextStyle(
            fontSize: 12
          )
        );
      case BlnSendType.lease:
        return Text("BLN "+S.of(context).lease,
          style: TextStyle(
            fontSize: 12
          )
        );
      case BlnSendType.transfer:
        return Text("BLN "+S.of(context).transfer,
          style: TextStyle(
            fontSize: 12
          )
        );
      default:
        return null;
    }
  }

  bool validator(){
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    if (!bln.verifyPassword(_textController.text)) {
      setState(() {
        _errorText = errorText(S.of(context).validationPassowrd);
      });
      return false;
    }
    return true;
  }
  
  Widget inputPassword() {
    return ListView(
      children: <Widget>[
        ListTile(
          title: TextFormField(
            controller: _textController,
            autofocus: false,
            decoration: InputDecoration(
              suffixIcon: IconButton(
                icon: Icon(_obscureText ? Icons.visibility_off : Icons.remove_red_eye),
                onPressed: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                }
              )
            ),
            maxLines: 1,
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 20.0),
            obscureText: _obscureText,
            cursorWidth: 2,
            cursorColor: Theme.of(context).accentColor
          ),
          subtitle: Container(
            child: _errorText,
            margin: EdgeInsets.only(top: 10),
          )
        )
      ],
    );
  }

  static Widget errorText([String text]){
    if (text != null) {
      return Text(text, 
        textAlign: TextAlign.left,
        style: TextStyles.textError14
      );
    }
    return Text("", 
          textAlign: TextAlign.left,
          style: TextStyles.textError14
        );
  }
}