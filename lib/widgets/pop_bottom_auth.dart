import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:blacknet/common/index.dart';

// BottomSheetAuth .
class BottomSheetAuth extends StatefulWidget {

  Function onCancelTap;
  Function onOkTap;
  Future<bool>Function([bool computed]) faceValidate;

  BottomSheetAuth({
    this.onCancelTap,
    this.onOkTap,
    this.faceValidate
  });
  @override
  _BottomSheetAuthState createState() => _BottomSheetAuthState();
}

class _BottomSheetAuthState extends State<BottomSheetAuth> {

  final TextEditingController _textController = TextEditingController();
  bool _nextStepStatus = false;
  Widget _errorText = errorText(null);
  final _formKey = GlobalKey<FormState>();
  bool _enableLocalAuth = true;
  bool _obscureText = true;
  bool _loading = true;

  @override
  void initState() {

    Future.delayed(Duration(milliseconds: 200)).then((v) async{
      setState(() {
        _loading = false;
      });
      Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
      if (_enableLocalAuth && !!bln.localAuth) {
        widget.faceValidate().then((valida){
        if(valida){
            widget.onOkTap();
          } else {
            setState(() {
              _enableLocalAuth = false;
            });
          }
        });
      }
    });

    // init form listener
    _textController.addListener((){
      setState(() {
        _nextStepStatus = _textController.text.length > 5;
        _errorText = errorText(null);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          AppBar(
            backgroundColor: Theme.of(context).bottomAppBarColor,
            title: navTitle(),
            titleSpacing: 0,
            automaticallyImplyLeading:false,
            elevation: 0,
            leading: navLeftButton(),
            actions: <Widget>[
              IconButton( 
                icon: new Icon(_enableLocalAuth && !!bln.localAuth ? Icons.security : Icons.face, color: Theme.of(context).accentColor),
                onPressed: () async {
                  if (_enableLocalAuth && !!bln.localAuth) {
                    // switch password
                    setState(() {
                      _enableLocalAuth = false;
                    });
                  } else {
                    // switch faceid
                    if (!bln.localAuth) {
                      // if dont open faceid， go to switch page
                      await OkNavigator.push(context, Routes.switchAuth);
                    }
                    setState(() {
                      _enableLocalAuth = !!bln.localAuth;
                    });
                    Future.delayed(Duration(milliseconds: 500), (){
                      if ( 
                          widget.faceValidate != null && 
                          widget.onOkTap != null && 
                          _enableLocalAuth &&
                          !!bln.localAuth
                      ) {
                        widget.faceValidate().then((valida){
                          if(valida){
                              widget.onOkTap();
                          } else {
                              setState(() {
                                _enableLocalAuth = false;
                              });
                          }
                        });
                      }
                    });
                  }          
                },
              )
            ],
          ),
          Expanded(
            flex: 1,
            child: (){
              if (_enableLocalAuth && !!bln.localAuth) {
                return showFacePage(null);
              }
              if (_loading) {
                return showLoadingPage();
              }
              return showPasswordPage();
            }(),
          ),
          Container(
            height: 50,
            padding: EdgeInsets.only(left: 15, right: 15, bottom: 10),
            child: nextButton()
          )
        ],
      ),
    );
  }

  Widget nextButton(){
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    if (_enableLocalAuth && !!bln.localAuth) {
      return Gaps.empty;
    }
    // password
    Function onTap;
    if (_nextStepStatus) {
      onTap = () async {
        if (validator() && widget.onOkTap != null) {
          widget.onOkTap();
        }
      };
    }
    return RaisedButton(
      child: Container(
        height: 40,
        width: double.infinity,
        alignment: Alignment.center,
        child: Text(S.of(context).confirm, style: TextStyle(fontSize: Dimens.font_sp18)),
      ), 
      onPressed: onTap
    );
  }

  Widget navLeftButton(){
    return IconButton( 
        icon: new Icon(Icons.cancel, color: Theme.of(context).accentColor),
        onPressed: () {
          widget.onCancelTap();
        }
    );
  }

  Widget navTitle(){
    return Text(S.of(context).verifyPassword);
  }


  bool validator(){
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    if (!bln.verifyPassword(_textController.text)) {
      setState(() {
        _errorText = errorText(S.of(context).validationPassowrd);
      });
      return false;
    }
    return true;
  }

  Widget showLoadingPage() {
    return Center(
      child: CircularProgressIndicator(strokeWidth: 3.0, valueColor: AlwaysStoppedAnimation<Color>(Colours.gold))
    );
  }

  Widget showFacePage(String text) {
    return Center(
      child: CircularProgressIndicator(strokeWidth: 3.0, valueColor: AlwaysStoppedAnimation<Color>(Colours.gold))
    );
  }
  
  Widget showPasswordPage() {
    return ListView(
      children: <Widget>[
        ListTile(
          title: TextFormField(
            controller: _textController,
            autofocus: false,
            decoration: InputDecoration(
              suffixIcon: IconButton(
                icon: Icon(_obscureText ? Icons.visibility_off : Icons.remove_red_eye),
                onPressed: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                }
              )
            ),
            maxLines: 1,
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 20.0),
            obscureText: _obscureText,
            cursorWidth: 2,
            cursorColor: Theme.of(context).accentColor
          ),
          subtitle: Container(
            child: _errorText,
            margin: EdgeInsets.only(top: 10),
          )
        )
      ],
    );
  }

  static Widget errorText([String text]){
    if (text != null) {
      return Text(text, 
        textAlign: TextAlign.left,
        style: TextStyles.textError14
      );
    }
    return Text("", 
          textAlign: TextAlign.left,
          style: TextStyles.textError14
        );
  }
}