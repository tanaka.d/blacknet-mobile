
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:blacknet/res/index.dart';
import 'package:blacknet/generated/i18n.dart';

typedef CallbackValidation = bool Function(List<String>);
typedef Callback = void Function();
typedef CallbackBool = void Function(bool);

class PasswordPayDialog extends StatefulWidget {
  final CallbackValidation onValidation;
  final Callback onPressed;
  final CallbackBool onCallback;
  final Stream<bool> shouldTriggerVerification;

  PasswordPayDialog(
      {Key key,
        @required this.onPressed,
        @required this.onValidation,
        @required this.onCallback,
        @required this.shouldTriggerVerification,
      }): super(key: key);
  @override
  _PasswordPayDialogState createState() => _PasswordPayDialogState();
}

class _PasswordPayDialogState extends State<PasswordPayDialog> {
  
  var _list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0];

  int _status = 0;// 0 default 1 transferloading 2 transfer success 3 transfer fail 4 password error

  StreamSubscription<bool> streamSubscription;

  @override
  initState() {
    super.initState();
    streamSubscription = widget.shouldTriggerVerification.listen((isValid) => _showValidation(isValid));
  }

  _showValidation(bool isValid) async{
    if (isValid) {
      setState(() {
        _status = 2;
      });
      await Future.delayed(Duration(milliseconds: 100));
      Navigator.pop(context);
      if(widget.onCallback!=null){
        widget.onCallback(true);
      }
    } else {
      setState(() {
        _status = 3;
      });
    }
  }

  void _validatePass() async{
    if(widget.onValidation != null && widget.onValidation(_codeList)){
      setState(() {
        _status = 1;
        _clear();
      });
      // todo send tx api
      if(widget.onPressed != null){
        await Future.delayed(Duration(milliseconds: 100));
        widget.onPressed();
      }
    }else{
      setState(() {
        _status = 4;
      });
    }
  }
  
  Widget _tips(){
    switch (_status) {
      case 3:
        return _tipError(S.of(context).transferFail);
        break;
      case 4:
        return _tipError(S.of(context).validationPassowrd);
        break;
      default:
        return Text(S.of(context).inputPassword, style: TextStyles.textBold18);
        // return Gaps.empty;
    }
  }

  Widget _passBox(){
    switch (_status) {
      case 1:
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Container(
              width: 45,
              height: 45,
              child: CircularProgressIndicator(backgroundColor: Theme.of(context).backgroundColor),
            )
          ],
        );
        break;
      case 2:
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Container(
              width: 60,
              height: 60,
              child: Icon(Icons.check_circle, color: Theme.of(context).accentColor, size: 60),
            )
          ],
        );
        break;
      default:
        return Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              height: 45.0,
              margin: const EdgeInsets.only(left: 16.0, right: 16.0),
              decoration: BoxDecoration(
                border: Border.all(width: 0.6, color: Colours.grey),
                borderRadius: BorderRadius.circular(4.0),
              ),
              child: Row(
                children: List.generate(_codeList.length, (i) => _buildInputWidget(i))
              ),
            )
          ],
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    // double height = MediaQuery.of(context).size.height * 7 / 10.0;
    double height = 500;
    return Material(
      type: MaterialType.transparency,
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              height: height,
              color: Theme.of(context).primaryColor,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    decoration: new BoxDecoration(
                      border: Border(bottom: BorderSide(color: Theme.of(context).dividerColor, width: 0.5))
                    ),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          alignment: Alignment.center,
                          padding: const EdgeInsets.symmetric(vertical: 12.0),
                          child: _tips(),
                          // child: Text(S.of(context).inputPassword, style: TextStyles.textBold18)
                        ),
                        Positioned(
                          right: 16.0,
                          top: 12.0,
                          bottom: 12.0,
                          child: InkWell(
                            onTap: (){
                              if(widget.onCallback!=null){
                                widget.onCallback(false);
                              }
                              Navigator.pop(context);
                            },
                            child: const SizedBox(
                              key: const Key('close'),
                              height: 16.0,
                              width: 16.0,
                              child: Icon(Icons.close)
                            ),
                          ),
                        )
                      ],
                    )
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        _passBox()
                      ],
                    ),
                  ),
                  Container(
                    color: Theme.of(context).dividerTheme.color,
                    decoration: new BoxDecoration(
                      border: Border(top: BorderSide(color: Theme.of(context).dividerColor, width: 0.5))
                    ),
                    child: GridView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        childAspectRatio: 1.953,
                        mainAxisSpacing: 0.6,
                        crossAxisSpacing: 0.6
                      ),
                      itemCount: 12,
                      itemBuilder: (_, index){
                        // final color = Colours.grey;
                        final color = null;
                        return Material(
                          color: (index == 9 || index == 11) ? color : null,
                          child: InkWell(
                            highlightColor: Theme.of(context).buttonColor,
                            splashColor: Theme.of(context).buttonColor.withOpacity(0.4),
                            child: Center(
                              child: index == 11 ? Icon(Icons.undo) : index == 9 ? Icon(Icons.clear) :
                              Text(_list[index].toString(), style: TextStyle(fontSize: 26.0)),
                            ),
                            onTap: (){
                              // status is loading 
                              if(_status == 1){
                                return;
                              }
                              // clear
                              if(index == 9){
                                setState(() {_status=0;_clear();});
                                return;
                              }
                              // undo
                              if(index == 11){
                                if (_index == 0){
                                  return;
                                }
                                setState(() {
                                  _codeList[_index - 1] = "";
                                  _index--;
                                  _status=0;
                                });
                                return;
                              }
                              // 输入超过输入框 重置
                              if(_index == _codeList.length){
                                _status=0;
                                _clear();
                              }
                              _codeList[_index] = _list[index].toString();
                              _index++;
                              if (_index == _codeList.length){
                                String code = "";
                                for (int i = 0; i < _codeList.length; i ++){
                                  code = code + _codeList[i];
                                }
                                // Toast.show("密码：$code");
                                _validatePass();
                              }
                              setState(() {});
                            },
                          ),
                        );
                      },
                    ),
                  ),
              ],
            ),
            )
          )
        ]
      )
    );
  }

  int _index = 0;
  List<String> _codeList = ["", "", "", "", "", ""];
  _clear(){
    _index = 0;
    for (int i = 0; i < _codeList.length; i ++){
      _codeList[i] = "";
    }
  }

  Widget _tipError(String text){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(Icons.warning, color: Theme.of(context).errorColor),
        Text(text, style: TextStyle(color: Theme.of(context).errorColor))
      ],
    );
  }

  Widget _buildInputWidget(int p){
    return Expanded(
      child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              border: p != 5 ? Border(
                right: Divider.createBorderSide(context, color: Colours.grey, width: 0.6),
              ) : null
          ),
          child: Text(_codeList[p].isEmpty ? "" : "●", style: TextStyle(color: Colours.grey))
      ),
    );
  }

  @override
  didUpdateWidget(PasswordPayDialog old) {
    super.didUpdateWidget(old);
    if (widget.shouldTriggerVerification != old.shouldTriggerVerification) {
      streamSubscription.cancel();
      streamSubscription = widget.shouldTriggerVerification.listen((isValid) => _showValidation(isValid));
    }
  }

  @override
  dispose() {
    streamSubscription.cancel();
    super.dispose();
  }
}