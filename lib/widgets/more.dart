import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:blacknet/res/index.dart';
class MoreWidget extends StatelessWidget {
  
  const MoreWidget(this.itemCount, this.hasMore, this.pageSize);

  final int itemCount;
  final bool hasMore;
  final int pageSize;
  
  @override
  Widget build(BuildContext context) {
    return !hasMore ? SizedBox(height: 0) : Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          hasMore ? const CupertinoActivityIndicator() : Gaps.empty,
          hasMore ? Gaps.hGap5 : Gaps.empty,
          /// 只有一页的时候，就不显示FooterView了
          Text(hasMore ? 'loading...' : (itemCount < pageSize ? '' : 'No More~')),
        ],
      ),
    );
  }
}