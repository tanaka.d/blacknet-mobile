

export 'package:blacknet/constants/resources.dart';



class Constants {
  /// debug开关，上线需要关闭
  /// App运行在Release环境时，inProduction为true；当App运行在Debug和Profile环境时，inProduction为false
  static const bool inProduction  = const bool.fromEnvironment("dart.vm.product");

  static bool isTest  = false;

  static String defaultTheme = "darker";
  // key 
  // account list
  static String accountList = "AppAccountList";
  // current account
  static String currentAccount = "AppCurrentAccount";
  // current balance
  static String currentBalance = "AppCurrentBalance";
  // theme
  static String theme = "AppTheme";
  // language
  static String language = "AppLanguage";
  // inActive
  static String appInActive = "AppInActive";

  // donate address
  static String donateAddress = "blacknet1y36epd3xh7vt2yv2yu7pnk06amfpyqlcc2jlkf4caznmc4xlgqgqc6deuj";
  // donate message
  static String donateMessage = "donate from blacknet mobile wallet";
  // gitlab url
  static String gitlab = "https://gitlab.com/blacknet-ninja/blacknet-mobile";
}