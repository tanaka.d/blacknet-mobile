import 'package:flutter/material.dart';

import 'package:blacknet/common/index.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

//页面加载器，作为主页面
class SplashWidget extends StatefulWidget {
  //生命周期函数
  @override
  SplashPageState createState() => SplashPageState();
}
 
 
//启动页面处理跳转页面逻辑
class SplashPageState extends State<SplashWidget> {
  @override
  void initState() {
    super.initState();
    // 存在当前用户 直接跳转
    Future.delayed(Duration(milliseconds: 500)).then((res){
      Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
      if (bln != null && !bln.isEmpty()) {
        OkNavigator.pushReplacementNamed(context, "${AccountRouter.accountUnlock}?address=${bln.address}&name=${bln.name}");
      }else{
        OkNavigator.pushReplacementNamed(context, Routes.index);
      }
    });
  }
 
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 414, height: 896, allowFontScaling: true)
          ..init(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Container(
          // alignment: Alignment.topCenter,
          //设置登录页面背景图片
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/1024.png"),
              fit: BoxFit.cover,
            )
          ),
          width: 150,
          height: 150,
        )
      ),
    );
  }
}
