import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:blacknet/provider/global.dart';
// i18n
import 'package:blacknet/generated/i18n.dart';
// theme
import 'package:blacknet/theme/index.dart';

class ThemePage extends StatefulWidget {

  @override
  _ThemePageState createState() => _ThemePageState();

}

class _ThemePageState extends State<ThemePage> {

  final List<Themes> themeDatas = ThemeDatas.supportThemes;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).theme)
      ),
      body: Padding(
        padding: const EdgeInsets.all(4.0),
        child: GridView.count(
          crossAxisCount: 4,
          children: List.generate(themeDatas.length, (i) {
            Themes themes = themeDatas[i];
            ThemeData themeData = ThemeDatas.supportThemeDatas[ThemeDatas.supportThemeMaps[themes]];
            return InkWell(
              onTap: () {
                Provider.of<GlobalProvider>(context).setTheme(themes);
              },
              child: Container(
                color: themeData.primaryColor,
                margin: const EdgeInsets.all(3.0),
              ),
            );
          }),
        )
      ),
    );
  }
}