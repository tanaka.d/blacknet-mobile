import 'package:flutter/material.dart';
import 'package:flustars/flustars.dart';
import 'dart:convert';

import 'package:blacknet/models/pos.dart';

class PosProvider with ChangeNotifier {
  // _hashrate
  void syncHashrate(){
    var hashStr = SpUtil.getString("pos_hashrate");
    if(hashStr != null && hashStr.isEmpty == false){
      notifyListeners();
    }
  }

  void setHashrate(PosWorkerHashrate h) {
    SpUtil.putString("pos_hashrate", h.toString());
    notifyListeners();
  }

  void setHashrateAsync(PosWorkerHashrate h) {
    SpUtil.putString("pos_hashrate", h.toString());
  }

  PosWorkerHashrate getHashrate() {
    var hashStr = SpUtil.getString("pos_hashrate");
    if(hashStr != null && hashStr.isEmpty == false){
      return PosWorkerHashrate.fromJson(jsonDecode(hashStr));
    }else{
      return PosWorkerHashrate();
    }
  }


  // PosList
  void syncPosList(){
    var txStrs = SpUtil.getStringList("pos_lists");
    if(txStrs != null && txStrs.isEmpty == false){
      notifyListeners();
    }
  }

  void setPosList(List<PosListItem> list) {
    SpUtil.putStringList("pos_lists", list.map((i) => jsonEncode(i)).toList());
    notifyListeners();
  }

  void setPosListAsync(List<PosListItem> list) {
    SpUtil.putStringList("pos_lists", list.map((i) => jsonEncode(i)).toList());
  }

  List<PosListItem> getPosList() {
    var txStrs = SpUtil.getStringList("pos_lists");
    return txStrs.map((i)=> PosListItem.fromJson(jsonDecode(i))).toList();
  }
   
}