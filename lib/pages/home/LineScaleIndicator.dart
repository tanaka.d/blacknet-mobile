import 'package:flutter/material.dart';
import 'package:loading/indicator.dart';

class LineScaleIndicator extends Indicator {
  var scaleYDoubles = [1.0, 1.0, 1.0, 1.0, 1.0];
    var controllers = List<AnimationController>();

  @override
  paint(Canvas canvas, Paint paint, Size size) {
    var translateX = size.width / 11;
    var translateY = size.height / 2;
    for (int i = 0; i < 5; i++) {
      canvas.save();
      canvas.translate((2 + i * 2) * translateX - translateX / 2, translateY);
      canvas.scale(1.0, scaleYDoubles[i]);
      var rectF = RRect.fromLTRBR(-translateX / 1, -size.height / 3.5,
          translateX / 4, size.height / 3.5, Radius.circular(0));
      canvas.drawRRect(rectF, paint);
      canvas.restore();
    }
  }

  @override
  List<AnimationController> animation() {
    for (int i = 0; i < 5; i++) {
      var sizeController = new AnimationController(
          duration: Duration(milliseconds: 1400), vsync: context);
      var alphaTween = new Tween(begin: 0, end: 0.99).animate(sizeController);
      sizeController.addListener(() {
        if (alphaTween.value <= 0.2) {
          scaleYDoubles[i] = 1 + 0.4 + 0.6 * alphaTween.value / 0.2;
        } else if (alphaTween.value < 0.4) {
          scaleYDoubles[i] = 1 - 0.4 + (0.4 - alphaTween.value) / 0.2;
        } else {
          scaleYDoubles[i] = 1;
        }

        postInvalidate();
      });
      controllers.add(sizeController);
    }
    return controllers;
  }

  @override
  startAnims(List<AnimationController> controllers) {
    var delays = [800, 900, 1000, 1100, 1200];
    for (var i = 0; i < controllers.length; i++) {
      Future.delayed(Duration(milliseconds: delays[i]), () {
        controllers[i].repeat(reverse: true);
      });
    }
  }

  @override
  void dispose() {
    controllers.forEach((c){
      c.stop();
      c.dispose();
    });
    super.dispose();
  }
}
