import 'package:blacknet/common/index.dart';
import 'package:blacknet/pages/home/info.dart';

class InfoPagePresenter extends BasePagePresenter<InfoPageState> {
  @override
  void initState() {
    getBlacknetInfo().then((data){
      SpHelper.putObject("blacknet_node", data[0]);
      SpHelper.putObject("blacknet_ledger", data[1]);
      SpHelper.putObject("blacknet_price", data[2]);
      Provider.of<DataCenterProvider>(view.context).notifyInfo();
    }).catchError((err){
      print(err);
    });
  }

  getBlacknetInfo() async {
    var futures = <Future>[];
    futures.addAll([
      BlnAPI.getNode(),
      BlnAPI.getLedger(),
      BlnAPI.getPrice()
    ]);
    return await Future.wait(futures);
  }

  onRefresh() async {
    return getBlacknetInfo().then((data){
      SpHelper.putObject("blacknet_node", data[0]);
      SpHelper.putObject("blacknet_ledger", data[1]);
      SpHelper.putObject("blacknet_price", data[2]);
      Provider.of<DataCenterProvider>(view.context).notifyInfo();
    }).catchError((err){
      print(err);
    });
  }

}