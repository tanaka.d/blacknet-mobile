
import 'package:flutter/material.dart';
// theme
import 'package:blacknet/theme/index.dart';


class AuthPage extends StatefulWidget {
  @override
  _AuthPage createState() => _AuthPage();
}

class _AuthPage extends State<AuthPage> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            new Expanded(
              flex: 2,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    width: ThemeLayout.width(160),
                    height: ThemeLayout.width(160),
                    child: new Image.asset(
                      'assets/images/logo.png',
                    )
                  ),
                  Container(
                    margin: ThemeLayout.marginTop(20),
                    child: new Column(
                      children: [
                      new Text('Syncing...', style: new TextStyle(
                      fontSize: ThemeLayout.fontSize(16), )),

                      Padding(
                        padding: const EdgeInsets.only(top: 30),
                        child: CircularProgressIndicator()
                      )
                    ])
                  )
                ],
              )
            ),
            new Expanded(
              flex: 2,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new Expanded(
                    flex: 2,
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        
                        SizedBox(height: ThemeLayout.height(20))
                        
                      ],
                    )
                  )
                ],
              ),
            )
          ]
        )
      )
    );
  }
}