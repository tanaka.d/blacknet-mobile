// import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:blacknet/common/index.dart';

import 'package:blacknet/pages/home/presenter/index.dart';
import 'package:oktoast/oktoast.dart';

class InfoPage extends StatefulWidget {
  @override
  InfoPageState createState() => InfoPageState();
}

class InfoPageState extends BasePageState<InfoPage, InfoPagePresenter>
    with
        AutomaticKeepAliveClientMixin<InfoPage>,
        SingleTickerProviderStateMixin {

  @override
  InfoPagePresenter createPresenter() {
    return InfoPagePresenter();
  }

  @override
  bool get wantKeepAlive => false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: onRefresh,
        displacement: 0, /// 默认40， 多添加的80为Header高度
        child: CustomScrollView(
          slivers: <Widget>[
            getSliverAppBar(),
            SliverList(delegate: SliverChildListDelegate([
              getBlockInfo(),
              getSocialInfo(),
              SizedBox(height: 30)
            ]))
          ],
        ),
      )
    );
  }

  Future<void> onRefresh() {
    return presenter.onRefresh();
  }

  SliverAppBar getSliverAppBar() {
    return SliverAppBar(
      backgroundColor: Theme.of(context).canvasColor,
      leading: Gaps.empty,
      elevation: 0.0,
      pinned: false
    );
  }

  Widget getSocialInfo(){
    return Card(
      margin: EdgeInsets.only(left: 4, right: 4, top: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(height: 15),
          ListTile(
            leading: Text("Source Code"),
            trailing: Text("Gitlab",
              style: TextStyle(
                color: Colours.blnType
              )
            ),
            onTap: ()async {
              try {
                await openLocalBrowser("https://gitlab.com/blacknet-ninja"); 
              } catch (e) {
                showToast(e.toString());
              }
            }
          ),
          ListTile(
            leading: Text("Community"),
            trailing: Text("#blacknet:matrix.org",
              style: TextStyle(
                color: Colours.blnType
              )
            ),
            onTap: ()async {
              try {
                await openLocalBrowser("https://riot.im/app/#/room/#blacknet:matrix.org"); 
              } catch (e) {
                showToast(e.toString());
              }
            }
          ),
          ListTile(
            trailing: Text("BitcoinTalk",
              style: TextStyle(
                color: Colours.blnType
              )
            ),
            onTap: ()async {
              try {
                await openLocalBrowser("https://bitcointalk.org/index.php?topic=469640.0"); 
              } catch (e) {
                showToast(e.toString());
              }
            }
          ),
          ListTile(
            trailing: Text("Reddit",
              style: TextStyle(
                color: Colours.blnType
              )
            ),
            onTap: ()async {
              try {
                await openLocalBrowser("https://www.reddit.com/r/blacknet"); 
              } catch (e) {
                showToast(e.toString());
              }
            }
          ),
          ListTile(
            trailing: Text("Activity in the GitLab group",
              style: TextStyle(
                color: Colours.blnType
              )
            ),
            onTap: ()async {
              try {
                await openLocalBrowser("https://gitlab.com/groups/blacknet-ninja/-/activity");
              } catch (e) {
                showToast(e.toString());
              }
            }
          ),
          ListTile(
            trailing: Text("QQgroup: 705702427",
              style: TextStyle(
                color: Colours.gold
              )
            )
          ),
          ListTile(
            leading: Text("Exchanges"),
            trailing: Text("BLN_CNC AEX.plus",
              style: TextStyle(
                color: Colours.blnType
              )
            ),
            onTap: ()async {
              try {
                await openLocalBrowser("https://www.aex.plus/page/trade.html?mk_type=CNC&trade_coin_name=BLN#/?symbol=bln_cnc"); 
              } catch (e) {
                showToast(e.toString());
              }
            }
          ),
          ListTile(
            trailing: Text("BTC_BLN vinex.network",
              style: TextStyle(
                color: Colours.blnType
              )
            ),
            onTap: ()async {
              try {
                await openLocalBrowser("https://vinex.network/market/BTC_BLN"); 
              } catch (e) {
                showToast(e.toString());
              }
            }
          )
        ],
      ),
    );
  }

  Widget getBlockInfo(){
    var blacknetNode = Provider.of<DataCenterProvider>(context).blacknetNode;
    var blacknetLedger = Provider.of<DataCenterProvider>(context).blacknetLedger;
    var blacknetPrice = Provider.of<DataCenterProvider>(context).blacknetPrice;
    var now = parseTime(blacknetLedger.blockTime);
    return Card(
      margin: EdgeInsets.only(left: 4, right: 4),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/images/1024.png'),
            ),
            trailing: Container(
              child: Text((blacknetPrice.range*100).toStringAsFixed(2)+"%",
                style: TextStyle(
                  color: Colors.white
                ),
              ),
              // color: Colors.redAccent,
              padding: EdgeInsets.only(left: 10, top: 5, bottom: 5, right: 10),
              decoration: BoxDecoration(
                color: blacknetPrice.range < 0 ? Colors.redAccent : Colors.greenAccent,
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0),
                ),
              )
            ),
            title: Text('Blacknet(BLN)'),
            subtitle: Text((blacknetPrice.last ).toStringAsFixed(8)+ ' CNY',
              style: TextStyle(
                color: Colours.gold
              )
            ),
          ),
          getItemTitle(
            left: textItemTitle(textAlign: TextAlign.left, title: "Block Height", content: blacknetLedger.height.toString()),
            right: textItemTitle(textAlign: TextAlign.right, title: "Block Time", content: formatDate(now))
          ),
          getItemTitleOne(
            title: "Difficulty", content: blacknetLedger.difficulty
          ),
          getItemTitleOne(
            title: "CumulativeDifficulty", content: blacknetLedger.cumulativeDifficulty
          ),  
          getItemTitle(
            left: textItemTitle(textAlign: TextAlign.left, title: "Version", content: blacknetNode.version),
            right: textItemTitle(textAlign: TextAlign.right, title: "ProtocolVersion", content: blacknetNode.protocolVersion.toString())
          ),
          getListTitle(
            left: "IBO Amount",
            right: "1,000,000,000 BLN"
          ),
          getListTitle(
            left: "IBO Price",
            right: "1 BLK = 67.99 BLN"
          ),
          getListTitle(
            left: "Total Burned",
            right: "14,708,159.7 BLK"
          ),
          getListTitle(
            left: "Genesis Block Time",
            right: "2018-12-13"
          ),
          getListTitle(
            left: "Supply",
            right: (BigInt.parse(blacknetLedger.supply) / BigInt.from(1e8)).toString()
          ),
        ],
      ),
    );
  }
  Widget getListTitle({
    String left,
    String right
  }){
    return Container(
      padding: EdgeInsets.only(left: 15, right: 15, bottom: 20),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(left)
            )
          ),
          Expanded(
            flex: 1,
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(right, 
                style: TextStyle(
                    color: Colours.gold
                )
              )
            )
          )
        ],
      )
    );
  }
  Widget getItemTitle({
    Widget left,
    Widget right
  }){
    return Container(
      padding: EdgeInsets.only(left: 15, right: 15, bottom: 20),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Align(
              alignment: Alignment.centerLeft,
              child: left
            )
          ),
          Expanded(
            flex: 1,
            child: Align(
              alignment: Alignment.centerRight,
              child: right
            )
          )
        ],
      )
    );
  }

  Widget getItemTitleOne({
    String title,
    String content
  }){
    return ListTile(
      title: Text(title),
      subtitle: Text(content,
        style: TextStyle(
          color: Colours.gold
        ),
        softWrap: true
      )
    );
  }

  Widget textItemTitle({
    TextAlign textAlign,
    String title,
    String content
  }){
    var crossAxisAlignment = WrapCrossAlignment.start;
    if (textAlign == TextAlign.center) {
      crossAxisAlignment = WrapCrossAlignment.center;
    }
    if (textAlign == TextAlign.right) {
      crossAxisAlignment = WrapCrossAlignment.end;
    }
    return Wrap(
      spacing: 5,
      crossAxisAlignment: crossAxisAlignment,
      direction: Axis.vertical,
      children: <Widget>[
        title != null ? Text(title): Gaps.empty,
        content != null ? Text(content,
          style: TextStyle(
            color: Colours.gold
          )
        ): Gaps.empty,
      ],
    );
  }

}
