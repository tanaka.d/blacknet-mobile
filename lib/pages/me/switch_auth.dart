import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:blacknet/common/index.dart';
import 'package:blacknet/pages/me/presenter/swtich_auth_presenter.dart';

class SwitchAuthPage extends StatefulWidget {
  @override
  SwitchAuthPageState createState() => SwitchAuthPageState();
}

class SwitchAuthPageState extends BasePageState<SwitchAuthPage, SwitchAuthPagePresenter>
    with
        AutomaticKeepAliveClientMixin<SwitchAuthPage>,
        SingleTickerProviderStateMixin {

  @override
  SwitchAuthPagePresenter createPresenter() {
    return SwitchAuthPagePresenter();
  }

  @override
  bool get wantKeepAlive => false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).secretFreePayment),
        elevation: 0,
        centerTitle: true
      ),
      body: Column(
        children: <Widget>[
          faceidLogo(),
          faceidDesc()
        ]
      )
    );
  }

  faceidLogo(){
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Align(
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            Container(
              child: new Image(
                  image: new AssetImage('assets/images/logo.png')),
            ),
            Container(
              margin: EdgeInsets.only(top: 30),
              color: Theme.of(context).bottomAppBarColor,
              child:
                ListTile(
                  leading: Text("Face ID / Touch ID", style: TextStyle(color: Theme.of(context).accentColor)),
                  trailing: Switch(value: bln.localAuth, onChanged: (v)async{
                    if(bln.localAuth && !(await showBottomAuth())){
                      return;
                    }
                    bln.localAuth = v;
                    if (!!v) {
                      String type = await getBiometricType();
                      if (type != null) {
                        bool allow = await showLocalAuth();
                        if (allow) {
                          bln.localAuth = !!allow;
                        }else{
                          bln.localAuth = false;
                        }
                      }else{
                        bln.localAuth = false;
                        showWarning(
                          Text(S.of(context).invalidDevice, textAlign: TextAlign.center),
                          Text(S.of(context).invalidDeviceInfo, textAlign: TextAlign.center)
                        );
                      }
                    }
                    await setCurrentBln(bln);
                  }),
                )
            )
          ],
        ),
      ),
    );
  }

  faceidDesc(){
    return Expanded(
      flex: 1,
      child: ListView(
        children: <Widget>[
          ListTile(
            title:
                Text(S.of(context).secretFreePayment1),
            subtitle: 
                Text(S.of(context).secretFreePayment2)
          ),
          ListTile(
            subtitle: 
                Text(S.of(context).secretFreePayment3)
          ),
          ListTile(
            title:
                Text(S.of(context).secretFreePayment4),
            subtitle: 
                Text(S.of(context).secretFreePayment5)
          ),
          ListTile(
            title:
                Text(S.of(context).secretFreePayment6),
            subtitle: 
                Text(S.of(context).secretFreePayment7)
          ),
        ]
      )
    );
  }
}
