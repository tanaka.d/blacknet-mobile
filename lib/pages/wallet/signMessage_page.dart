import 'dart:async';
import 'package:blacknet/pages/wallet/presenter/transfer_presenter.dart';
import 'package:blacknet/res/gaps.dart';
import 'package:blacknet/services/LocalAuthenticationService.dart';
import 'package:blacknet/services/service_locator.dart';
import 'package:blacknet/utils/toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:blacknet/widgets/index.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'package:blacknet/common/index.dart';
import 'package:blacknet_lib/blacknet.dart';

class SignMessagePage extends StatefulWidget {
  @override
  SignMessagePageState createState() => SignMessagePageState();
}

class SignMessagePageState extends BasePageState<SignMessagePage, SignMessagePagePresenter>
    with
        AutomaticKeepAliveClientMixin<SignMessagePage>,
        SingleTickerProviderStateMixin {

  @override
  SignMessagePagePresenter createPresenter() {
    return SignMessagePagePresenter();
  }


  @override
  bool get wantKeepAlive => false;

  final TextEditingController _amountController = TextEditingController();
  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();
  final LocalAuthenticationService _localAuth = locator<LocalAuthenticationService>();

  final _formKey = GlobalKey<FormState>();
  var message = '';
  var hexSign = '';
  @override
  void initState() {
    super.initState();
  }

  Widget get textArea {
    return TextFormField(
        autofocus: false,
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(10),
            border: InputBorder.none,
            focusedBorder: InputBorder.none),
        maxLines: 3,
        autovalidate: true,
        onChanged: (val) {
          message = val;
        },
        validator: (value) {
          return null;
        });
  }

  Widget get textAreaTitle {
    return Container(
        color: Theme.of(context).canvasColor,
        child: ListTile(
            title: Container(
                padding: const EdgeInsets.only(top: 1),
                child: Text(S.of(context).signMessageTitle))));
  }

  void sign() async {

    setState(() {
      hexSign = '';
    });
    if(await showBottomAuth()){
      showProgress();
      Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
      var hex = await bln.sign(message).catchError((err){
        print(err);
      });
      setState(() {
        hexSign = hex;
      });
      closeProgress();
    }
  }

  Widget get signButton {
    return Container(
        margin: EdgeInsets.only(top: 40),
        padding: EdgeInsets.only(left: 15, right: 15),
        child: MyButton(
            key: Key("Sign"), text: S.of(context).sign, onPressed: sign));
  }

  Widget get pageForm {
    return Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Card(
              margin: const EdgeInsets.only(top: 25),
              elevation: 3,
              child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                textAreaTitle,
                textArea,
              ]),
            )
          ],
        ));
  }

  Widget get resultPanel {
    return Card(
      margin: const EdgeInsets.only(top: 25),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            trailing: Icon(Icons.content_copy,
                color: Theme.of(context).accentColor, size: 16),
            title: Container(
              padding: const EdgeInsets.only(top: 10),
              child: Text("Signature"),
            ),
            subtitle: Container(
              padding: const EdgeInsets.only(top: 10, bottom: 10),
              child: Text(hexSign),
            ),
            onTap: () {
              Clipboard.setData(new ClipboardData(text: hexSign)).then((v) {
                Toast.show(S.of(context).copySuccess);
              });
            },
          )
        ],
      ),
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
            AppBar(
              title: Text(S.of(context).signMessage), 
              centerTitle: true,
              elevation: 0,
              backgroundColor: Theme.of(context).canvasColor
            ),
        body: MyPage(
            child: ListView(children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0),
              child: pageForm),
          signButton,
          hexSign.length > 0 ? resultPanel : Gaps.empty
        ])));
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    // pass
    _verificationNotifier.close();
    _amountController.dispose();
    super.dispose();
  }
}
