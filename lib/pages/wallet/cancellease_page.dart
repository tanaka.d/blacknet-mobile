import 'dart:async';
import 'package:blacknet/components/index.dart';
import 'package:blacknet/services/LocalAuthenticationService.dart';
import 'package:blacknet/services/service_locator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// apis
import 'package:blacknet/components/api/api.dart';
import 'package:blacknet/common/index.dart';
// widgets
import 'package:blacknet/widgets/index.dart';
import 'package:blacknet/pages/wallet/presenter/transfer_presenter.dart';

class CancelLeasePage extends StatefulWidget {
  const CancelLeasePage({
    Key key,
    @required this.address,
  }) : super(key: key);
  final String address;
  @override
  CancelLeasePageState createState() => CancelLeasePageState();
}

class CancelLeasePageState extends BasePageState<CancelLeasePage, CancelLeasePagePresenter>
    with
        AutomaticKeepAliveClientMixin<CancelLeasePage>,
        SingleTickerProviderStateMixin {

  @override
  CancelLeasePagePresenter createPresenter() {
    return CancelLeasePagePresenter();
  }

  @override
  bool get wantKeepAlive => true;

  ScrollController _scrollController = ScrollController();
  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();

  final LocalAuthenticationService _localAuth =
      locator<LocalAuthenticationService>();

  List<BlnLease> _list;
  double fee = 0.001;
  double amount;
  String to;
  String msg;
  bool encrypted = false;
  int height = 0;
  StateType _stateType = StateType.empty;
  String _address;

  @override
  void initState() {
    super.initState();
    _address = widget.address;
    // _onRefresh();
  }

  Future<void> sendCancelLease(BuildContext context, BlnLease bl)async{
      
      // bool checkResult = await _localAuth.auth('Cancelling');
      // if (checkResult) {
      //   Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
      //   var mnemonic = await bln.getMnemonic();

        // var res = await Api.cancelLease(mnemonic, fee,
        //     bl.amount.toDouble() / 1e8, bl.publicKey, bl.height);
        
      //   dismissProgressDialog();
      //   if(res.body.length == 64){

      //     _list.remove(bl);
      //     Provider.of<AccountProvider>(context).setCancelLease(_list);
      //   }else{
      //     Toast.show(res.body, duration: 2000);
      //   }
       
      // }else{
      //   dismissProgressDialog();
      // }

      String res = await showBottomPay(
        type: BlnSendType.cancelLease,
        amount: bl.amount.toDouble() / 1e8,
        to: bl.publicKey,
        height: bl.height
      );
      if (res != "false") {
        _list.remove(bl);
        Provider.of<AccountProvider>(context).setCancelLease(_list);
      }
  }

  Widget _renderRow(BuildContext context, BlnLease bl) {
    return new ListTile(
      contentPadding: ThemeLayout.padding(10, 5, 10, 5),
      // subtitle: Text(shortAddress(bl.publicKey, 12)),
      title: Text(fomartBalance(bl.amount.toDouble()) + " BLN",
          style: Theme.of(context).textTheme.display2),
      // subtitle: Text(bl.height.toString()),
      leading: Container(
          width: 80,
          child: Column(children: <Widget>[
            Align(
                child: Text(bl.height.toString()),
                alignment: Alignment.centerLeft)
          ], mainAxisAlignment: MainAxisAlignment.center)),
      trailing: new IconButton(
          // action button
          // color: Theme.of(context).textTheme.display3.color,
          icon: Icon(Icons.cancel),
          onPressed: () async{
            await sendCancelLease(context, bl);
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    _list = Provider.of<AccountProvider>(context).getCancelLease();
    return Scaffold(
      appBar:
          AppBar(
            title: Text(S.of(context).cancelLease), 
            centerTitle: true,
            elevation: 0,
            backgroundColor: Theme.of(context).canvasColor
          ),
      body: NotificationListener(
        child: RefreshIndicator(
          //下拉刷新触发方法
          onRefresh: getPresenter().onRefresh,
          child: CustomScrollView(
            controller: _scrollController,
            key: PageStorageKey<String>("cancellease"),
            physics: AlwaysScrollableScrollPhysics(),
            slivers: <Widget>[
              SliverPadding(
                padding: const EdgeInsets.symmetric(horizontal: 0.0),
                sliver: _list == null || _list.isEmpty
                    ? SliverFillRemaining(
                        child: _list != null && _list.isEmpty
                            ? StateLayout.empty(S.of(context).noOutLeases)
                            : StateLayout(type: _stateType))
                    : SliverList(
                        delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) {
                          return index < _list.length
                              ? _renderRow(context, _list[index])
                              : MoreWidget(_list.length, false, 1000000000);
                        }, childCount: _list.length + 1),
                      ),
              )
            ],
          )
        )
      )
    );
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }
}
