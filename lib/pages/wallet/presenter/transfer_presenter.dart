import 'package:flutter/material.dart';
import 'package:blacknet/mvp/mvp.dart';
import 'package:blacknet/pages/wallet/transfer_page.dart';
import 'package:blacknet/pages/wallet/lease_page.dart';
import 'package:blacknet/pages/wallet/cancellease_page.dart';
import 'package:blacknet/pages/wallet/signMessage_page.dart';
 
import 'package:blacknet/common/index.dart';
import 'package:blacknet/components/api/api.dart';

class SignMessagePagePresenter extends BasePagePresenter<SignMessagePageState> {

}


class TransferPagePresenter extends BasePagePresenter<TransferPageState> {
  @override
  void initState() async {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await Provider.of<DataCenterProvider>(view.context).refershBalance();
    });
  }
}

class LeasePagePresenter extends BasePagePresenter<LeasePageState> {
  @override
  void initState() async {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await Provider.of<DataCenterProvider>(view.context).refershBalance();
    });
  }
}

class CancelLeasePagePresenter extends BasePagePresenter<CancelLeasePageState> {
  @override
  void initState() async {
    Api.outLeases(view.widget.address).then((ls){
      Provider.of<AccountProvider>(view.context).setCancelLease(ls);
    }).catchError((err){
      print(err);
    });
  }

  // 刷新
  Future<Null> onRefresh() async {
    var ls = await Api.outLeases(view.widget.address);
    if (view.mounted) {
      Provider.of<AccountProvider>(view.context).setCancelLease(ls);
    }
  }
}