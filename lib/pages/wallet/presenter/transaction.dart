import 'package:blacknet/mvp/mvp.dart';
import 'package:blacknet/pages/wallet/transaction_list.dart';

import 'package:blacknet/common/index.dart';
import 'package:blacknet/components/api/api.dart';

class TransactionListPresenter extends BasePagePresenter<TransactionListState> {
  @override
  void initState() async {
    // view.showProgress();
    // view.onRefresh();
    refershTxnsByType().then((txns){
      Provider.of<DataCenterProvider>(view.context).setRefershTxnsByType(view.widget.type, txns);
    }).catchError((err){
      print(err);
    });
  }

  Future<List<BlnTxns>> refershTxnsByType() async {
    var address = view.widget.address;
    var type = view.widget.type;
    if (type != null)
      return await Api.getTxns(address, 1, type);
    else
      return await Api.getTxns(address, 1);
  }
}