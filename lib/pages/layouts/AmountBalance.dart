import 'package:blacknet/utils/bln.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_deer/util/theme_utils.dart';

class AmountBalance extends StatelessWidget {
  var amount;

  AmountBalance(this.text, this.amount, this.onPressed);

  final String text;
  final VoidCallback onPressed;

  void unfocusText(context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(text),
          FlatButton(
              padding: const EdgeInsets.all(0),
              onPressed: this.onPressed,
              child: Text(fomartBalance(amount) + ' BLN',
                  style: TextStyle(
                      color: Theme.of(context).textTheme.display3.color)))
        ]);
  }
}
