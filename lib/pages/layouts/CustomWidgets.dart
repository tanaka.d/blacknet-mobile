


import 'package:blacknet/generated/i18n.dart';
import 'package:blacknet/utils/index.dart';
import 'package:flutter/material.dart';

class CustomWidgets {

  static Widget getTextInputInstance(context, skey,  controller,  hintText, realAmount,  onSaved,  onChanged){
    return Container(
        child: TextFormField(
          key: Key(skey.toString()),
          autocorrect: true,
          controller: controller,
          autofocus: false,
          keyboardType: TextInputType.numberWithOptions(decimal: true),
          style: TextStyle(
            textBaseline: TextBaseline.alphabetic,
            color: Theme.of(context).textTheme.display4.color
          ),
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
            border: InputBorder.none,
            filled: true,
            fillColor: Theme.of(context).textTheme.body1.color,
            hintText: hintText
          ),
          onSaved: onSaved,
          onChanged: onChanged,
          validator: (value) {
            if(value==null){
              return null;
            }
            if(!validatorBalance(value)){
              return S.of(context).amountInputCorrect;
            }
            if(value.length > 0 && double.parse(value) > realAmount){
              return S.of(context).amountInputEnough;
            }
            return null;
          },
          autovalidate: true
    ));
  }

}

