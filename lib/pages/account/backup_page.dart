import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
// i8n
import 'package:blacknet/generated/i18n.dart';
// utils
import 'package:blacknet/utils/toast.dart';

// ruters
import 'package:blacknet/routers/routers.dart';
import 'package:blacknet/widgets/index.dart';

class AccountBackupPage extends StatefulWidget {
  String address;
  String password;
  String mnemonic;
  AccountBackupPage({this.address, this.mnemonic, this.password});
  @override
  _AccountBackupPage createState() => _AccountBackupPage();
}

class _AccountBackupPage extends State<AccountBackupPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(S.of(context).backupMnemonic),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () { 
                OkNavigator.goBack(context);
              },
              iconSize: 24,
            );
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Card(
              margin: const EdgeInsets.only(top: 25),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    trailing: Icon(Icons.content_copy, color: Theme.of(context).accentColor, size: 16),
                    title: Container(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(S.of(context).mnemonic),
                    ),
                    subtitle: Container(
                      padding: const EdgeInsets.only(top: 10,bottom: 10),
                      child: Text(widget.mnemonic),
                    ),
                    onTap: (){
                      Clipboard.setData(new ClipboardData(text: widget.mnemonic)).then((v){
                        Toast.show(S.of(context).copySuccess);
                      });
                    },
                  )
                ],
              ),
            ),
            Card(
              margin: const EdgeInsets.only(top: 25),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    trailing: Icon(Icons.content_copy, color: Theme.of(context).accentColor, size: 16),
                    title: Container(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(S.of(context).address),
                    ),
                    subtitle: Container(
                      padding: const EdgeInsets.only(top: 10,bottom: 10),
                      child: Text(widget.address),
                    ),
                    onTap: (){
                      Clipboard.setData(new ClipboardData(text: widget.address)).then((v){
                        Toast.show(S.of(context).copySuccess);
                      });
                    },
                  )
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 25),
              child: MyButton(
                key: Key("use"),
                text: S.of(context).done,
                onPressed: () async {
                  OkNavigator.goBack(context);
                }
              )
            )
          ],
        )
      )
    );
  }
}