import 'package:flutter/material.dart';

class CreatePageProvider with ChangeNotifier {
    bool _nextStatus = false;
    int get nextStatus => nextStatus;
    void refresh(){
      notifyListeners();
    }
    void setNextStatus(bool s) {
      _nextStatus = s;
      notifyListeners();
    }
}