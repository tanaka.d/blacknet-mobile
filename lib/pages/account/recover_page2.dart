import 'package:flutter/material.dart';
// routers
import 'package:blacknet/routers/routers.dart';
// i8n
import 'package:blacknet/generated/i18n.dart';

import 'package:blacknet/pages/account/widgets/password.dart';

class AccountRecoverPage2 extends StatefulWidget {
  String address;
  String password;
  String mnemonic;
  AccountRecoverPage2({this.address, this.mnemonic, this.password});
  @override
  _AccountRecoverPage2 createState() => _AccountRecoverPage2();
}

class _AccountRecoverPage2 extends State<AccountRecoverPage2> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(S.of(context).importWallet),
            Text(S.of(context).importStep2, style: TextStyle(fontSize: 14))
          ],
        )
      ),
      // backgroundColor: Theme.of(context).accentColor,
      body: PasswordSettingWidget(
        text: <Widget>[
          Text(S.of(context).settingPassowrd, style: TextStyle(fontSize: 20))
        ],
        buttonText: S.of(context).nextStep,
        onPressed: (List<String> codes){
          String password = codes.join("");
          OkNavigator.push(context, '${Routes.accountImport3}?address=${widget.address}&password=${password}&mnemonic=${widget.mnemonic}');
        }
      )
    );
  }
}