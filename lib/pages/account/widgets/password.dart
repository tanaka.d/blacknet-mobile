
import 'package:blacknet/theme/index.dart';
import 'package:flutter/material.dart';
import 'package:blacknet/res/index.dart';
import 'package:blacknet/widgets/index.dart';
import 'package:blacknet/generated/i18n.dart';

typedef Callback = void Function(List<String>);

class PasswordSettingWidget extends StatefulWidget {
  final List<Widget> text;
  final String buttonText;
  final Callback onPressed;

  PasswordSettingWidget(
      {Key key,
        @required this.text,
        @required this.onPressed,
        @required this.buttonText,
      }): super(key: key);
  @override
  _PasswordSettingWidgetState createState() => _PasswordSettingWidgetState();
}

class _PasswordSettingWidgetState extends State<PasswordSettingWidget> {
  
  var _list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0];
  
  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: this.widget.text == null ? []: this.widget.text,
                  // children: <Widget>[
                  //   Text(("密码不可为连续、重复的数字。"), style: Theme.of(context).textTheme.subtitle),
                  // ],
                )
              )
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 45.0,
                    margin: const EdgeInsets.only(left: 16.0, right: 16.0),
                    decoration: BoxDecoration(
                      border: Border.all(width: 0.6, color: Colours.grey),
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    child: Row(
                      children: List.generate(_codeList.length, (i) => _buildInputWidget(i))
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 16.0, right: 16.0, top: 20.0),
                    child: MyButton(
                      // key: Key("nextStep"),
                      text: this.widget.buttonText == null ? S.of(context).nextStep : this.widget.buttonText,
                      onPressed: (_index < 6 ? null : (){
                        if(this.widget.onPressed != null){
                          this.widget.onPressed(_codeList);
                        }
                      })
                    ),
                  )
                ],
              ),
            ),
            // Gaps.line,
            Divider(height: 2),
            Container(
              color: Theme.of(context).dividerTheme.color,
              child: GridView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  childAspectRatio: 1.953,
                  mainAxisSpacing: 0.6,
                  crossAxisSpacing: 0.6
                ),
                itemCount: 12,
                itemBuilder: (_, index){
                  // final color = Colours.grey;
                  final color = null;
                  return Material(
                    color: (index == 9 || index == 11) ? color : null,
                    child: InkWell(
                      highlightColor: Theme.of(context).buttonColor,
                      splashColor: Theme.of(context).buttonColor.withOpacity(0.4),
                      child: Center(
                        child: index == 11 ? Icon(Icons.undo) : index == 9 ? Icon(Icons.clear) :
                        Text(_list[index].toString(), style: TextStyle(fontSize: 26.0)),
                      ),
                      onTap: (){
                        // clear
                        if(index == 9){
                          setState(() {_clear();});
                          return;
                        }
                        // undo
                        if(index == 11){
                          if (_index == 0){
                            return;
                          }
                          setState(() {
                            _codeList[_index - 1] = "";
                            _index--;
                          });
                          return;
                        }
                        // 输入超过输入框 重置
                        if(_index == _codeList.length){
                          _clear();
                        }
                        _codeList[_index] = _list[index].toString();
                        _index++;
                        if (_index == _codeList.length){
                          String code = "";
                          for (int i = 0; i < _codeList.length; i ++){
                            code = code + _codeList[i];
                          }
                          // Toast.show("密码：$code");
                        }
                        setState(() {});
                      },
                    ),
                  );
                },
              ),
            ),
        ],
      ),
    );
  }

  int _index = 0;
  List<String> _codeList = ["", "", "", "", "", ""];
  _clear(){
    _index = 0;
    for (int i = 0; i < _codeList.length; i ++){
      _codeList[i] = "";
    }
  }
  Widget _buildInputWidget(int p){
    return Expanded(
      child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              border: p != 5 ? Border(
                right: Divider.createBorderSide(context, color: Colours.grey, width: 0.6),
              ) : null
          ),
          child: Text(_codeList[p].isEmpty ? "" : "●", style: TextStyle(color: Colours.grey))
      ),
    );
  }
}